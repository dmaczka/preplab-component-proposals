# PrepLab Prototype Proposal

This collection of project proposals is intended to describe the core components of the PrepLab prototype, a digital repository of instructional material intended for graduate student instructors to archive their teaching material, present it in a teaching portfolio, and support the re-use and re-mixing of open access material.

## Storage and Discovery

This component involves the design of a storage and retreival system that will meet the needs of the PrepLab project. The suggested starting point is to use git as a storage mechanism for content as this will preserve authorship and revision information, which will be necessary for the eventual re-use and re-mixing of material. We think that assigning one git repo per re-usable/re-mixable chunk of data will make the most sense, but most users will think of their data as documents made up of one or more chunks. To facilitate searching across chunks we think creating a NoSQL database representation of the documents will make this process easier. The database could be made to stay in sync with the git repo via commit hooks.

## Profiles and Portfolios

One of the core functions of PrepLab will be to allow instructors to currate a professional profile of themselves and a teaching portfolio that could be shared with potential collaborators or employers.

## Parsing and Indexing

Based on our initial round of surveys to potential users, ease of getting material into the system is of critical importance in instructors adopting it into their workflow. This poses a challenge with the larger goals of allowing for re-use and re-mixing as most instructors are not creating content with these uses in mind. 